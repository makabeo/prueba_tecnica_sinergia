<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    protected $table = 'municipios';

    protected $fillable = ['id','nombre','estado','departamento_id'];

    public function departamento()
    {
        return $this->belongsTo('App\Departamento');
    }
}
