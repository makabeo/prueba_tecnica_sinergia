<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class PuntoUnoController extends Controller
{
    /**
     * Muestra numero mas repetido de un arreglo de numeros
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function repetido(Request $request)
    {
        $texto = $request->arreglo;
        $arreglo = explode(",", $texto);
        $arreglo = array_count_values($arreglo);
        Alert::info('Punto 1 A', 'El valor que mas se repite es: ' . array_keys($arreglo)[0])->persistent(true,true);
        return redirect('/');
    }

    /**
     * Muestra si la palabra escrita es un palindromo
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function palindromo(Request $request)
    {
        $palindromo = $request->palindromo;
        $invertido = strrev($palindromo);
        $noCoincidencias = 0;
        for($i=0;$i<strlen ($palindromo);$i++)
        {
            if($palindromo[$i] !== $invertido[$i])
            {
                $noCoincidencias++;
            }
        }


        if ($noCoincidencias === 0)
        {
            Alert::info('Punto 1 B', 'El valor es un palindromo perfecto')->persistent(true,true);
        }
        else if ($noCoincidencias < 2)
        {
            Alert::info('Punto 1 B', 'El valor es un palindromo con ' . $noCoincidencias . 'no coincidencias')->persistent(true,true);
        }
        else
        {
            Alert::info('Punto 1 B', 'El valor no es un palindromo tiene mas de 2 no coincidencias')->persistent(true,true);
        }
        return redirect('/');
    }
}
